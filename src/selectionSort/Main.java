package selectionSort;

/**
 * Created by marcin on 08.08.17.
 */
public class Main {
    public static void main(String[] args) {
        int[] arr1 = {10,34,2,56,7,67,88,42};
        int[] arr2 = doSelectionSort(arr1);
        for(int i:arr2){
            System.out.print(i);
            System.out.print(", ");
        }
    }

    private static int[] doSelectionSort(int[] arr) {
        int temp=0;
        for (int i = 0; i < arr.length - 1; i++) {
            int smallestNumberIndex=i;
            for (int j = i+1; j <arr.length ; j++) {
                if (arr[j]<arr[smallestNumberIndex]){
                    smallestNumberIndex=j;
                }
            }
            temp=arr[i];
            arr[i]=arr[smallestNumberIndex];
            arr[smallestNumberIndex]=temp;
        }
    return arr;
    }
}
