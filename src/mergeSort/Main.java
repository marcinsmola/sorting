package mergeSort;

/**
 * Created by marcin on 09.08.17.
 */
public class Main {
    public static void main(String[] args) {
        int[] arr1 = {10,34,2,56,7,67,88,42,1};
        doMergeSort(arr1);
        for(int i:arr1){
            System.out.print(i);
            System.out.print(", ");
        }
    }

    private static int[] mergeArrays(int[] left, int[] rigth,int[] arrayToSort){
        int i = 0,j = 0;
        int indexOfPointer=0;
        while (i<left.length && j< rigth.length){
            if (left[i]<=rigth[j]){
                arrayToSort[indexOfPointer]=left[i];
                i++;
                indexOfPointer++;
            }else {
                arrayToSort[indexOfPointer]=rigth[j];
                j++;
                indexOfPointer++;
            }
        }
        while (i<left.length){
            arrayToSort[indexOfPointer]=left[i];
            i++;
            indexOfPointer++;
        }
        while (j<rigth.length){
            arrayToSort[indexOfPointer]=rigth[j];
            j++;
            indexOfPointer++;
        }
        return arrayToSort;
    }

    private static int[] doMergeSort(int[] arr1) {
        if (arr1.length<2){
            return arr1;
        }
        int mid=arr1.length/2;
        int[] left=new int[mid];
        int[] right=new int[arr1.length-mid];
        for (int i = 0; i < mid; i++) {
            left[i]=arr1[i];
        }
        for (int i = mid; i <arr1.length ; i++) {
            right[i-mid]=arr1[i];
        }
        doMergeSort(left);
        doMergeSort(right);
        mergeArrays(left,right,arr1);
        return arr1;
    }
}
